﻿# Projet NF26 - P19 - UTC

Authors : Lorys Hamadache et Romain Creuzenet

## Organisation générale du projet
- livrables : contient le rapport PDF et le Diapo PDF
- scripts : contient le code python concernant le projet

## Comment utiliser le projet
- Pour installer le projet :
	télécharcher toutes les librairies python listées dans requirements.txt

- Pour initialiser les données (la première fois quand les tables sont vides):
  1. Executer download_data.py qui va télécharger tous les fichiers CSV nécessaires
  2. Executer create_table.py qui va créer et remplir les tables avec le contenu des fichiers CSV téléchargés

- Pour exploiter l'un des 3 objectif du projet:
	exécuter main.py. Une interface dans le terminal va vous guider

## Objectifs du projets
- Pour un point donné de l’espace, je veux pouvoir avoir un historique du passé, avec des courbes adaptés. Je vous pouvoir mettre en évidence la saisonnalité et les écarts à la saisonnalité.

- À un instant donné je veux pouvoir obtenir une carte me représentant n’importe quel indicateur.

- Pour une période de temps donnée, je veux pouvoir obtenir clusteriser l’espace, et représenter cette clusterisation.

## Architecture précise du projet
- Le fichier « requierments.txt » :
Ce fichier permet d’obtenir toutes les librairies à installer pour faire fonctionner le projet.
- livrables : contient le rapport PDF et le Diapo PDF
- scripts : contient le code python concernant le projet
  - Le fichier « parameters.py » :
	Il permet de stoker la configuration du projet (pays concerné, période de temps étudiée…). Il possède également les informations utiles à tout le projet.

  - Le fichier « download_data.py » :
Ce fichier permet de télécharger les données du pays concerné sur la période temps étudiée depuis le site internet où elles sont stockées. Ces informations seront ensuite placées dans des fichiers « csv » par station dans le dossier « out ». Le code est inspiré de celui trouvé sur le site internet gardant les données. Ce fichier n’est utilisé que très rarement, à une initialisation. Il suffit de l’exécuter pour appeler les bonnes fonctions.

  - Le fichier « create_table.py » :
Ce fichier crée les différentes tables utilisées durant le projet et les remplie des informations contenues dans tous les fichiers « csv » du dossier « out ». Ce fichier n’est utilisé que très rarement, à une initialisation. Il suffit de l’exécuter pour appeler les bonnes fonctions.

  - Le fichier « main.py » :
Ce fichier gère l’exploitation des données. Il est utilisé fréquemment. Son exécution permet d’obtenir une interface dans le terminal pour choisir son objectif, les éléments nécessaires et obtenir les résultats.

- Le dossier « data » :
Ce dossier contient tous les fichiers « csv » contenant toutes les informations, sur les différentes stations, qui devront être mises dans Cassandra. Ce dossier n’est pas censé être exploité manuellement par l’utilisateur.

- Le dossier « imgs » :
Ce dossier contient toutes les images utilisées pour les rapports
