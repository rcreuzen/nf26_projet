"""
    Script to download all data from Spain to 2001 at 2010
"""
from __future__ import print_function
from urllib.request import urlopen
from parameters import DIR_DATA, START, END, NETWORK
import json
import time
import os


# Number of attempts to download data
MAX_ATTEMPTS = 6
# HTTPS here can be problematic for installs that don't have Lets Encrypt CA
SERVICE = "http://mesonet.agron.iastate.edu/cgi-bin/request/asos.py?"


def download_data(uri):
    """Fetch the data from the IEM
    The IEM download service has some protections in place to keep the number
    of inbound requests in check.  This function implements an exponential
    backoff to keep individual downloads from erroring.
    Args:
      uri (string): URL to fetch
    Returns:
      string data
    """
    attempt = 0
    while attempt < MAX_ATTEMPTS:
        try:
            data = urlopen(uri, timeout=300).read().decode('utf-8')
            if data is not None and not data.startswith('ERROR'):
                return data
        except Exception as exp:
            print("download_data(%s) failed with %s" % (uri, exp))
            time.sleep(5)
        attempt += 1

    print("Exhausted attempts to download, returning empty data")
    return ""


def get_stations_from_networks():
    """Build a station list by using a bunch of IEM networks."""
    stations = []

    # Get metadata
    uri = ("https://mesonet.agron.iastate.edu/"
           "geojson/network/%s.geojson") % (NETWORK,)
    data = urlopen(uri)
    jdict = json.load(data)
    for site in jdict['features']:
        stations.append(site['properties']['sid'])
    return stations


def main():
    """Our main method"""

    service = SERVICE + "data=all&tz=Etc%2FUTC&format=onlycomma&latlon=yes&missing=null&trace=null&direct=no&" \
                        "report_type=2&"
    service += START.strftime('year1=%Y&month1=%m&day1=%d&')
    service += END.strftime('year2=%Y&month2=%m&day2=%d&')

    stations = get_stations_from_networks()

    # prepare data folder
    os.makedirs(DIR_DATA, exist_ok=True)
    for file in os.listdir(DIR_DATA):
        file_path = os.path.join(DIR_DATA, file)
        os.remove(file_path)

    n = len(stations)
    for i, station in enumerate(stations):
        uri = '%s&station=%s' % (service, station)
        print('%d / %d Downloading: %s' % (i, n, station))
        data = download_data(uri)
        filename = '%s_%d_%d.csv' % (station, START.year, END.year)
        path = os.path.join(DIR_DATA, filename)
        print("\t->", path)
        with open(path, 'w') as file:
            file.write(data)


if __name__ == '__main__':
    main()
