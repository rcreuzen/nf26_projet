from parameters import TABLE, DIR_DATA, SESSION, COLUMNS, TABLES
import csv
import re
import os


def create_table():
    """ Create the table"""
    for table, primary_key in TABLES.items():
        SESSION.execute("DROP Table IF EXISTS {};".format(table))
        translate_sql = {
            str: 'text',
            int: 'varint',
            float: 'float'
        }
        query = """
        CREATE TABLE {table}(
            time tuple<int,int,int, int, int>,
            {columns},
            PRIMARY KEY ({primary_key})
        );
        """.format(
            table=table,
            columns=",\n\t".join(["{} {}".format(key, translate_sql[value]) for key, value in COLUMNS.items()]),
            primary_key=primary_key
        )
        SESSION.execute(query)


def read_csv():
    """Read CSV file"""
    date_parser = re.compile(
        r"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+)"
    )
    for file_name in os.listdir(DIR_DATA):
        file_path = os.path.join(DIR_DATA, file_name)
        with open(file_path) as file:
            for row in csv.DictReader(file):
                match = date_parser.match(row["valid"])
                if not match:
                    continue
                m = match.groupdict()
                r = {key: f(row[key]) for key, f in COLUMNS.items() if row.get(key, 'null') != 'null'}
                r["time"] = (int(m['year']), int(m['month']), int(m['day']), int(m['hour']), int(m['minute']))
                yield r


def insert_table():
    """Insert in table all content csv file"""
    i = 0
    for d in read_csv():
        keys = []
        values = []
        for key, value in d.items():
            keys.append(key)
            values.append(value)

        keys = ", ".join(keys)
        values = ", ".join([v.__repr__() for v in values])

        for table in TABLES.keys():
            query = "INSERT INTO {table} ({keys}) VALUES ({values});".format(
                table=table,
                keys=keys,
                values=values
            )
            SESSION.execute(query)
        i += 1
        print("Ligne {}".format(i), end="\r")
    print("{} lignes inserted".format(i))


if __name__ == "__main__":
    create_table()
    insert_table()
