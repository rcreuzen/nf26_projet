"""
    Parameters to use project.
    For each cumputers
"""
from cassandra.cluster import Cluster, NoHostAvailable
from collections import OrderedDict
import os
import datetime
# Can be specified
KEY_SPACE = ("lhamadac_projet", "nf26")  # For many computers
TABLE = "Spain"
ATTRIBUTS = {
    "tmpf": "La témparature (en Fahrenheit)",
    "relh": "L'humidité ( en %)",
    "alti": "Altimètre de pression en pouces"
}

# Don't change
SESSION = None
for key in KEY_SPACE:
    try:
        SESSION = Cluster(['localhost']).connect(key)
    except NoHostAvailable:
        pass
    else:
        break
if SESSION is None:
    raise NoHostAvailable("Erreur de connection à cassandra", None)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # path of project
DIR_DATA = os.path.join(BASE_DIR, "data")  # folder with all data station
DIR_OUT = os.path.join(BASE_DIR, "out")  # folder with all graphics
NETWORK = "ES__ASOS"  # Change
START = datetime.datetime(2011, 1, 1)  # 1 janvier 2001
END = datetime.datetime(2013, 12, 31)  # 31 décembre 2010
COLUMNS = OrderedDict([
    ('station', str),
    ('lon', float),
    ('lat', float),
    ('tmpf', float),
    ('tmpc', float),
    ('dwpf', float),
    ('dwpc', float),
    ('relh', float),
    ('drct', float),
    ('sknt', float),
    ('sped', float),
    ('alti', float),
    ('mslp', float),
    # ('p01m',float), all null
    # ('p01i',float), all null
    ('vsby', float),
    ('gust', float),
    ('skyc1', str),
    ('skyc2', str),
    ('skyc3', str),
    ('skyc4', str),
    ('skyl1', float),
    ('skyl2', float),
    ('skyl3', float),
    ('skyl4', float),
    ('wxcodes', str),
    # ('ice_accretion_1hr', ), all null
    # ('ice_accretion_3hr', ), all null
    # ('ice_accretion_6hr', ), all null
    # ('peak_wind_gust', ), all null
    # ('peak_wind_drct', ), all null
    # ('peak_wind_time', ), all null
    ('feel', float)
])
# Name of table -> Primary key
TABLES = {
    'TABLE_SPACE': "station, time",
    'TABLE_TIME': "time, lon, lat",
}


